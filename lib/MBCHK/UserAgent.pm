#+##############################################################################
#                                                                              #
# LWP::UserAgent subclass that better deals with authentication                #
#                                                                              #
#-##############################################################################

package MBCHK::UserAgent;

use strict;
use warnings qw(FATAL all);
use Authen::Credential qw();
use LWP::UserAgent;

our(@ISA);

@ISA = qw(LWP::UserAgent);

#
# constructor
#

sub new : method {
    my($class, $auth) = @_;
    my(%option, $self);

    if ($auth->scheme() eq "x509") {
        $option{ssl_opts} = {
            SSL_ca_path     => $auth->ca(),
            SSL_cert_file   => $auth->cert(),
            SSL_key_file    => $auth->key(),
            SSL_verify_mode => 1,
            verify_hostname => 1,
        };
    }
    $self = $class->SUPER::new(%option);
    $self->{auth} = $auth;
    return($self);
}

#
# authentication helper
#

sub get_basic_credentials : method {
    my($self, $realm, $uri, $isproxy) = @_;
    my($auth);

    $auth = $self->{auth};
    return() unless $auth->scheme() eq "plain";
    return($auth->name(), $auth->pass());
}

1;
