#+##############################################################################
#                                                                              #
# File: MBCHK.pm                                                               #
#                                                                              #
# Description: common code for message broker checks                           #
#                                                                              #
#-##############################################################################

#
# module definition
#

package MBCHK;
use strict;
use warnings qw(FATAL all);

#
# modules
#

use threads;
use threads::shared;
use Config::General qw(ParseConfig);
use Config::Validator qw(*);
use Directory::Queue qw();
use Getopt::Long qw(GetOptions);
use JSON qw();
use No::Worries qw($ProgramName);
use No::Worries::Die qw(dief);
use No::Worries::Dir qw(dir_ensure dir_parent);
use No::Worries::Export qw(export_control);
use No::Worries::Log qw(log_debug log_filter);
use No::Worries::PidFile qw(*);
use No::Worries::Proc qw(proc_detach);
use No::Worries::Syslog qw(syslog_open syslog_close);
use No::Worries::Warn qw(warnf);
use Pod::Usage qw(pod2usage);
use Thread::Queue qw();
use Time::HiRes qw();

#
# constants
#

use constant THREAD_OPTIONS => { void => 1, stack_size => 64 * 1024 };

#
# variables
#

our(%Config, $JSON, %NeedsCleanup, %Option, %Schema);
our($SchedulerQueue, $WorkerQueue, $WriterQueue);
our(@WorkerThreads, $WriterThread);

#
# start a thread receiving check results and writing them in a directory queue
#

sub _writer () {
    my($active, $dirq, $result, $json);

    $active = 1;
    local $SIG{TERM} = sub { $active = 0 };
    log_debug("starting writer...");
    $dirq = Directory::Queue->new(
        path => $Config{queue},
        type => "Simple",
    );
    while ($active) {
        $result = $WriterQueue->dequeue();
        last unless $result;
        $json = $JSON->encode($result);
        $dirq->add($json);
        log_debug("enqueued %s", $json) if $Option{debug} > 1;
    }
    log_debug("stopping writer...");
}

sub thread_start_writer () {
    $WriterThread = threads->create(THREAD_OPTIONS, \&_writer);
}

#
# start a thread receiving work definitions and using them to do something
#

sub _worker_code ($) {
    my($code) = @_;

    return(sub {
        my($active, $work, $result);

        $active = 1;
        local $SIG{TERM} = sub { $active = 0 };
        log_debug("starting worker...");
        while ($active) {
            $work = $WorkerQueue->dequeue();
            last unless $work;
            $result = $code->($work);
            if ($result) {
                $WriterQueue->enqueue($result);
                $work->{time} = Time::HiRes::time() + $Config{interval};
                $SchedulerQueue->enqueue($work) unless $Option{once};
            }
        }
        log_debug("stopping worker...");
    });
}

sub thread_start_worker ($) {
    my($code) = @_;

    push(@WorkerThreads, threads->create(THREAD_OPTIONS, _worker_code($code)));
}

#
# detect dead threads
#

sub thread_deads () {
    my($count);

    $count = 0;
    foreach my $thread (threads->list(threads::joinable)) {
        warnf("joining unexpected dead thread: %d", $thread->tid());
        $thread->join();
        $count++;
    }
    return($count);
}

#
# handle the --daemon option
#

sub handle_daemon () {
    $No::Worries::Log::Handler = \&No::Worries::Syslog::log2syslog;
    $No::Worries::Die::Syslog = 1;
    $No::Worries::Warn::Syslog = 1;
    syslog_open(ident => $ProgramName, facility => "user");
    $NeedsCleanup{syslog}++;
    proc_detach(callback => sub {
        printf("%s (pid %d) started\n", $ProgramName, $_[0])
    });
    log_debug("daemonized");
}

#
# handle the --pidfile option
#

sub handle_pidfile () {
    dir_ensure(dir_parent($Option{pidfile}));
    pf_set($Option{pidfile});
    $NeedsCleanup{pidfile}++;
}

#
# handle the --quit option
#

sub handle_quit () {
    dief("missing mandatory option for --quit: --pidfile")
        unless $Option{pidfile};
    pf_quit($Option{pidfile},
        linger   => $Option{linger},
        callback => sub { printf("%s %s\n", $ProgramName, $_[0]) },
    );
    exit(0);
}

#
# handle the --status option
#

sub handle_status () {
    my($status, $message, $code);

    dief("missing mandatory option for --status: --pidfile")
        unless $Option{pidfile};
    ($status, $message, $code) =
        pf_status($Option{pidfile}, freshness => $Option{linger});
    printf("%s %s\n", $ProgramName, $message);
    exit($code);
}

#
# extend the schema with standard options used in this module
#

sub extend_schema () {
    $Schema{config}{fields}{daemon} ||= {
        type     => "boolean",
        optional => "true",
    };
    $Schema{config}{fields}{interval} ||= {
        type     => "integer",
        optional => "true",
    };
    $Schema{config}{fields}{queue} ||= {
        type     => "string",
        optional => "false",
    };
    $Schema{config}{fields}{threads} ||= {
        type     => "integer",
        optional => "true",
    };
}

#
# load the configuration file (and set %Config)
#

sub load_config () {
    my($validator);

    dief("missing option: --config") unless $Option{config};
    %Config = ParseConfig(
        -CComments       => 0,
        -ConfigFile      => $Option{config},
        -IncludeRelative => 1,
        -IncludeGlob     => 1,
    );
    foreach my $name (qw(interval queue threads)) {
        $Config{$name} = $Option{$name} if defined($Option{$name});
    }
    $Config{daemon} = $Option{daemon} ? "true" : "false"
        if defined($Option{daemon});
    log_debug("Config = %s", $JSON->encode(\%Config))
        if $Option{debug} > 2;
    extend_schema();
    $validator = Config::Validator->new(%Schema);
    $validator->validate(\%Config, "config");
    $Config{interval} = 60 unless defined($Config{interval});
    $Config{threads} = 1 unless defined($Config{threads});
}

#
# initialisation
#

sub init ($$) {
    my($prepare, $setup) = @_;

    $| = 1;
    $Option{debug} = 0;
    $Option{linger} = 10;
    Getopt::Long::Configure(qw(posix_default no_ignore_case));
    GetOptions(\%Option,
        "config|conf=s",
        "daemon!",
        "debug|d+",
        "help|h|?",
        "interval=i",
        "linger=i",
        "manual|m",
        "once",
        "pidfile=s",
        "queue=s",
        "quit",
        "status",
        "threads=i",
    ) or pod2usage(2);
    pod2usage(1) if $Option{help};
    pod2usage(exitstatus => 0, verbose => 2) if $Option{manual};
    pod2usage(2) if @ARGV;
    log_filter("debug") if $Option{debug};
    handle_quit() if $Option{quit};
    handle_status() if $Option{status};
    $JSON = JSON->new()->utf8(1)->convert_blessed(1)->allow_blessed(1);
    load_config();
    $SchedulerQueue = Thread::Queue->new();
    $WorkerQueue = Thread::Queue->new();
    $WriterQueue = Thread::Queue->new();
    log_debug("preparing...");
    if ($Config{check}) {
        foreach my $check (listof($Config{check})) {
            $prepare->($check);
        }
        $Config{check} = shared_clone($Config{check});
    } else {
        $prepare->();
    }
    handle_daemon() if is_true($Config{daemon});
    handle_pidfile() if $Option{pidfile};
    $setup->();
    if ($Config{check}) {
        foreach my $check (listof($Config{check})) {
            $check->{time} = Time::HiRes::time();
            $SchedulerQueue->enqueue($check);
        }
    }
}

#
# what to do while main is idle
#

sub idle () {
    return("child thread died") if thread_deads();
    if ($Option{pidfile}) {
        return("told to quit") if pf_check($Option{pidfile}) eq "quit";
        pf_touch($Option{pidfile});
    }
    return();
}

#
# main loop (until ^C or --quit) for scheduled checks
#

sub mainchk () {
    my($termination, $check, $sleep);

    local $SIG{INT}  = sub { $termination = "caught SIGINT" };
    local $SIG{QUIT} = sub { $termination = "caught SIGQUIT" };
    local $SIG{TERM} = sub { $termination = "caught SIGTERM" };
    while (not $termination) {
        $check = $SchedulerQueue->dequeue_timed(1);
        if ($check) {
            while (not $termination) {
                $sleep = $check->{time} - Time::HiRes::time();
                last if $sleep <= 0;
                $sleep = 1 if $sleep > 1;
                Time::HiRes::sleep($sleep);
                $termination ||= idle();
            }
            $WorkerQueue->enqueue($check) unless $termination;
        } elsif ($Option{once}) {
            $termination = "once only";
        } else {
            Time::HiRes::sleep(1);
            $termination ||= idle();
        }
    }
    log_debug($termination) if $termination;
}

#
# general purpose main loop (until ^C or --quit)
#

sub main ($) {
    my($code) = @_;
    my($termination, $busy);

    local $SIG{INT}  = sub { $termination = "caught SIGINT" };
    local $SIG{QUIT} = sub { $termination = "caught SIGQUIT" };
    local $SIG{TERM} = sub { $termination = "caught SIGTERM" };
    while (not $termination) {
        $busy = $code->();
        if ($busy) {
            $termination ||= idle();
        } elsif ($Option{once}) {
            $termination = "once only";
        } else {
            Time::HiRes::sleep(1);
            $termination ||= idle();
        }
    }
    log_debug($termination) if $termination;
}

#
# cleaning up (high-level)
#

sub cleanup () {
    my(@running);

    log_debug("cleaning...");
    # kindly ask all worker threads to stop
    foreach my $thread (@WorkerThreads) {
        $WorkerQueue->enqueue(0);
    }
    if ($Option{once}) {
        # wait for all the worker threads to end
        while (@WorkerThreads) {
            Time::HiRes::sleep(0.1);
            @running = ();
            foreach my $thread (@WorkerThreads) {
                if ($thread->is_joinable()) {
                    $thread->join();
                } else {
                    push(@running, $thread);
                }
            }
            @WorkerThreads = @running;
        }
    }
    # kindly ask the writer thread to stop
    if ($WriterThread) {
        $WriterQueue->enqueue(0);
        Time::HiRes::sleep(0.1);
    }
    # kill and detach the worker threads that are still running at this point
    foreach my $thread (@WorkerThreads) {
        next if $thread->is_joinable();
        $thread->kill("TERM")->detach();
    }
    # join the remaining threads
    foreach my $thread (threads->list()) {
        $thread->join();
    }
}

#
# cleaning up (low level)
#

END {
    return if $No::Worries::Proc::Transient;
    log_debug("ending...");
    pf_unset($Option{pidfile}) if $NeedsCleanup{pidfile};
    syslog_close() if $NeedsCleanup{syslog};
}

#
# export control
#

sub import : method {
    my($pkg, %exported);

    $pkg = shift(@_);
    grep($exported{$_}++, qw(%Config $JSON %Option %Schema $WorkerQueue));
    export_control(scalar(caller()), $pkg, \%exported, @_);
}

1;
