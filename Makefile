PKGNAME=mbchk
PKGVERSION=$(shell cat VERSION)
PKGTAG=v${PKGVERSION}
FULLPKGNAME=${PKGNAME}-${PKGVERSION}
DISTDIR=dist/${FULLPKGNAME}
TARBALL=${FULLPKGNAME}.tgz

.PHONY: sources tag rpm srpm clean

${TARBALL}:
	rm -fr dist
	mkdir -p ${DISTDIR}
	cp -a README VERSION Makefile ${PKGNAME}.spec bin etc lib systemd ${DISTDIR}
	cd dist; tar cvfz ../${TARBALL} ${FULLPKGNAME}

sources: ${TARBALL}

tag:
	@seen=`git tag -l | grep -Fx ${PKGTAG}`; \
	if [ "x$$seen" = "x" ]; then \
	    set -x; \
	    git tag ${PKGTAG}; \
	    git push --tags; \
	else \
	    echo "already tagged with ${PKGTAG}"; \
	fi

rpm: ${TARBALL}
	rpmbuild -ta ${TARBALL}

srpm: ${TARBALL}
	rpmbuild -ts ${TARBALL}

clean:
	rm -fr dist ${TARBALL}
