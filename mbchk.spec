%define real_version %((cat %{_sourcedir}/VERSION || cat %{_builddir}/VERSION || echo UNKNOWN) 2>/dev/null)
%global etc_dir      /etc/mbchk
%global run_dir      /run/mbchk
%global spool_dir    /var/spool/mbchk

Name:		mbchk
Version:	%{real_version}
Release:	1%{?dist}
Summary:	Message Broker Checking
License:	Apache-2.0
URL:		https://gitlab.cern.ch/mig/%{name}
Source0:	%{name}-%{version}.tgz
Source1:	VERSION
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root
BuildArch:	noarch
BuildRequires:	perl-generators, systemd-rpm-macros

%description
This is the Message Broker Checking (MBCHK) package. Its purpose is to execute
external checks (i.e. not metric based) and send the results to CERN's MONIT.

%prep
%setup -q -n %{name}-%{version}

%build

%install
rm -fr %{buildroot}
# scripts
install -m 0755 -d %{buildroot}%{_bindir}
for name in bin/*; do
  install -m 0755 $name %{buildroot}%{_bindir}/
done
# libraries
install -m 0755 -d %{buildroot}%{perl_vendorlib}/MBCHK
install -m 0644 lib/MBCHK.pm %{buildroot}%{perl_vendorlib}/
for name in lib/MBCHK/*.pm; do
  install -m 0644 $name %{buildroot}%{perl_vendorlib}/MBCHK/
done
# configuration
install -m 0755 -d %{buildroot}%{etc_dir}
install -m 0755 -d %{buildroot}%{etc_dir}/dnschk.d
install -m 0755 -d %{buildroot}%{etc_dir}/httpchk.d
install -m 0755 -d %{buildroot}%{etc_dir}/stompchk.d
for name in etc/*.conf; do
  install -m 0644 $name %{buildroot}%{etc_dir}/
done
# systemd
install -m 0755 -d %{buildroot}%{run_dir}
install -m 0755 -d %{buildroot}%{_unitdir}
for name in systemd/*; do
  install -m 0644 $name %{buildroot}%{_unitdir}/
done
# miscellaneous
install -m 0700 -d %{buildroot}%{spool_dir}

%clean
rm -rf %{buildroot}

%post
%systemd_post chk2monit.service
%systemd_post dnschk.service
%systemd_post httpchk.service
%systemd_post stompchk.service

%preun
%systemd_preun chk2monit.service
%systemd_preun dnschk.service
%systemd_preun httpchk.service
%systemd_preun stompchk.service
if [ $1 = 0 ]; then
  rm -f %{etc_dir}/*.d/*.conf %{run_dir}/*
  rm -fr %{spool_dir}/*
fi

%files
%defattr(-,root,root,-)
%doc README
%{_bindir}/*
%{perl_vendorlib}/MBCHK.pm
%{perl_vendorlib}/MBCHK
%{etc_dir}
%{run_dir}
%{_unitdir}/*
%{spool_dir}
